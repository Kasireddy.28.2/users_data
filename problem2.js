function usersStayingInGermany(object){
    let users_staying_in_germany=[];
    for(let user in object){
        for(let key in object[user]){
            if(key==="nationality" && object[user][key]==="Germany"){
                users_staying_in_germany.push(user);
            }
        }
    }

    return users_staying_in_germany;

}

module.exports=usersStayingInGermany;
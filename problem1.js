
function usersInterestedInPlayingVideoGames(object){
        let users_interested_in_playing_video_games=[];
        for(let user in object){
            for(let key in object[user]){
                if(key==="interests" && object[user][key].toString().includes("Playing Video Games")){
                    users_interested_in_playing_video_games.push(user);
                };
            };
        };
    
        return users_interested_in_playing_video_games;
    

};

module.exports=usersInterestedInPlayingVideoGames;
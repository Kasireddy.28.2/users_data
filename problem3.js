function usersWithMastersDegree(object){
    let users_with_masters_degree=[];
    for(let user in object){
        for(let key in object[user]){
            if(key="qualification" && object[user][key]==="Masters"){
                users_with_masters_degree.push(user);
            }
        }
    }
    return users_with_masters_degree;
}

module.exports=usersWithMastersDegree;